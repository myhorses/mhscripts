## mhScripts

Scripts to install and configure applications.  

Do not use these scripts because it can damage your system.  

~~~
git clone https://gitlab.com/myhorses/mhscripts.git
cd mhscripts
chmod +x -Rv */*.sh
~~~

This is provided "AS IS", without warranty of any kind.  

---

Copyright (c) 2021-2024 Raymond Risko