#!/bin/bash

if [ ! -e /home/user/Desktop ]; then
  if [ -e /home/user/Área\ de\ trabalho ]; then
    mv -fv /home/user/Área\ de\ trabalho /home/user/Desktop
  fi
fi

if [ ! -e /home/user/Desktop ]; then
  if [ -e /home/user/Área\ de\ Trabalho ]; then
    mv -fv /home/user/Área\ de\ Trabalho /home/user/Desktop
  fi
fi

if [ ! -e /home/user/Desktop ]; then
  mkdir -v /home/user/Desktop
fi

if [ -e /home/user/Desktop ]; then

  if [ -e /home/user/Documents ]; then
    rm -rfv /home/user/Documents
  fi
  if [ -e /home/user/Music ]; then
    rm -rfv /home/user/Music
  fi;
  if [ -e /home/user/Pictures ]; then
    rm -rfv /home/user/Pictures
  fi
  if [ -e /home/user/Public ]; then
    mv -fv /home/user/Public /home/user/Shared
  fi
  if [ -e /home/user/Templates ]; then
    rm -rfv /home/user/Templates
  fi
  if [ -e /home/user/Videos ]; then
    rm -rfv /home/user/Videos
  fi

  if [ -e /home/user/Documentos ]; then
    rm -rfv /home/user/Documentos
  fi
  if [ -e /home/user/Imagens ]; then
    rm -rfv /home/user/Imagens
  fi
  if [ -e /home/user/Modelos ]; then
    rm -rfv /home/user/Modelos
  fi
  if [ -e /home/user/Música ]; then
    rm -rfv /home/user/Música
  fi
  if [ -e /home/user/Músicas ]; then
    rm -rfv /home/user/Músicas
  fi;
  if [ -e /home/user/Público ]; then
    mv -fv /home/user/Público /home/user/Shared
  fi
  if [ -e /home/user/Vídeos ]; then
    rm -rfv /home/user/Vídeos
  fi


  ## put here dirs to delete in others languages


  if [ ! -e /home/user/Downloads ]; then
    mkdir -v /home/user/Downloads
  fi
  if [ ! -e /home/user/Shared ]; then
    mkdir -v /home/user/Shared
  fi

  echo 'XDG_DESKTOP_DIR="$HOME/Desktop"' > /home/user/.config/user-dirs.dirs
  echo 'XDG_DOWNLOAD_DIR="$HOME/Downloads"' >> /home/user/.config/user-dirs.dirs
  echo 'XDG_TEMPLATES_DIR="$HOME"' >> /home/user/.config/user-dirs.dirs
  echo 'XDG_PUBLICSHARE_DIR="$HOME/Shared"' >> /home/user/.config/user-dirs.dirs
  echo 'XDG_DOCUMENTS_DIR="$HOME"' >> /home/user/.config/user-dirs.dirs
  echo 'XDG_MUSIC_DIR="$HOME"' >> /home/user/.config/user-dirs.dirs
  echo 'XDG_PICTURES_DIR="$HOME"' >> /home/user/.config/user-dirs.dirs
  echo 'XDG_VIDEOS_DIR="$HOME"' >> /home/user/.config/user-dirs.dirs
  
  echo 'Ok, set user dirs config in: /home/user/.config/user-dirs.dirs'
else 
  echo 'Error, fail set user dirs.'
  exit 1
fi

exit 0

