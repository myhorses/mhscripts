#!/bin/bash

if [ $EUID -ne 0 ]; then
  echo 'Error, this script need be run as root.'
  exit 1
fi

../1-essential/101-apt-sources_configure.sh
vscript_return=$?
if [ $vscript_return -ne 0 ]; then
  exit vscript_return
fi

echo ''
../1-essential/102-shell-essential_install.sh
vscript_return=$?
if [ $vscript_return -ne 0 ]; then
  exit vscript_return
fi

echo ''
../1-essential/103-kernel-headers_install.sh
vscript_return=$?
if [ $vscript_return -ne 0 ]; then
  exit vscript_return
fi

echo ''
../1-essential/104-libs-build_install.sh
vscript_return=$?
if [ $vscript_return -ne 0 ]; then
  exit vscript_return
fi

echo ''
../1-essential/105-nonfree-drivers_install.sh
vscript_return=$?
if [ $vscript_return -ne 0 ]; then
  exit vscript_return
fi

echo ''
../1-essential/106-file-tools_install.sh
vscript_return=$?
if [ $vscript_return -ne 0 ]; then
  exit vscript_return
fi

echo ''
../1-essential/107-net-tools_install.sh;
vscript_return=$?
if [ $vscript_return -ne 0 ]; then
  exit vscript_return
fi;

echo ''
../1-essential/108-shell-tools_install.sh
vscript_return=$?
if [ $vscript_return -ne 0 ]; then
  exit vscript_return
fi

echo ''
../2-dev/202-sqlite_install.sh
vscript_return=$?
if [ $vscript_return -ne 0 ]; then
  exit vscript_return
fi

echo ''
echo 'Done, system base setups run successfully.'

exit 0

