#!/bin/bash

if [ $EUID -ne 0 ]; then
  echo 'Error, this script need be run as root.'
  exit 1
fi

apt update
vAPTupdate_return=$?
if [ $vAPTupdate_return -eq 0 ]; then
  echo 'APT update test success.'
else
  echo "Error, APT update return is: $vAPTupdate_return"
  exit $vAPTupdate_return
fi

echo ''
varch=$(dpkg --print-architecture)

if [ $varch == "i386" ]; then
  apt -y install linux-headers-686 linux-image-686
  vAPTinstall_return=$?
elif [ $varch == "amd64" ]; then
  apt -y install linux-headers-amd64 linux-image-amd64
  vAPTinstall_return=$?
else
  echo "Error, the architecture '$varch' is not supported by this script."
  exit 1
fi

exit $vAPTinstall_return

