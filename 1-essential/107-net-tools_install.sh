#!/bin/bash

if [ $EUID -ne 0 ]; then
  echo 'Error, this script need be run as root.'
  exit 1
fi

apt update
vAPTupdate_return=$?
if [ $vAPTupdate_return -eq 0 ]; then
  echo 'APT update test success.'
else
  echo "Error, APT update return is: $vAPTupdate_return"
  exit $vAPTupdate_return
fi

echo ''
apt -y install network-manager
vAPTinstall_return=$?
if [ $vAPTinstall_return -ne 0 ]; then
  exit $vAPTinstall_return
fi

echo ''
apt -y install net-tools
vAPTinstall_return=$?
if [ $vAPTinstall_return -ne 0 ]; then
  exit $vAPTinstall_return
fi

echo ''
apt -y install libssl-dev
vAPTinstall_return=$?;
if [ $vAPTinstall_return -ne 0 ]; then
  exit $vAPTinstall_return
fi

echo ''
apt -y install lsb-release ca-certificates
vAPTinstall_return=$?;
if [ $vAPTinstall_return -ne 0 ]; then
  exit $vAPTinstall_return
fi

echo ''
apt -y install curl wget
vAPTinstall_return=$?
if [ $vAPTinstall_return -ne 0 ]; then
  exit $vAPTinstall_return
fi

echo ''
apt -y install openssh-client
vAPTinstall_return=$?
if [ $vAPTinstall_return -ne 0 ]; then
  exit $vAPTinstall_return
fi

echo ''
apt -y install links
vAPTinstall_return=$?
if [ $vAPTinstall_return -ne 0 ]; then
  exit $vAPTinstall_return
fi

exit 0

