#!/bin/bash

if [ $EUID -ne 0 ]; then
  echo 'Error, this script need be run as root.'
  exit 1
fi

vDISTcodename=$(cat /etc/os-release | grep VERSION_CODENAME | cut -d= -f2)

if [ $vDISTcodename == "bookworm" ]; then

  echo 'Rewriting: /etc/apt/sources.list'
  echo '' > /etc/apt/sources.list
  echo "deb http://deb.debian.org/debian $vDISTcodename main non-free non-free-firmware contrib" >> /etc/apt/sources.list
  echo '' >> /etc/apt/sources.list
  
elif [ $vDISTcodename == "bullseye" ] || [ $vDISTcodename == "buster" ]; then

  echo 'Rewriting: /etc/apt/sources.list'
  echo '' > /etc/apt/sources.list
  echo "deb http://deb.debian.org/debian $vDISTcodename main non-free contrib" >> /etc/apt/sources.list
  echo '' >> /etc/apt/sources.list

else
  echo "Error, the Debian version '$vDISTcodename' is not supported by this script."
  exit 1
fi

rm -fv /etc/apt/sources.list.d/*

apt update
vAPTupdate_return=$?
if [ $vAPTupdate_return -eq 0 ]; then
  echo 'APT update test success.'
else
  echo "Error, APT update return is: $vAPTupdate_return"
  exit $vAPTupdate_return
fi

exit 0

