#!/bin/bash

if [ $EUID -ne 0 ]; then
  echo 'Error, this script need be run as root.'
  exit 1
fi

apt update
vAPTupdate_return=$?
if [ $vAPTupdate_return -eq 0 ]; then
  echo 'APT update test success.'
else
  echo "Error, APT update return is: $vAPTupdate_return"
  exit $vAPTupdate_return
fi

echo ''
apt -y install libext2fs2
vAPTinstall_return=$?
if [ $vAPTinstall_return -ne 0 ]; then
  exit $vAPTinstall_return
fi

echo ''
apt -y install reiserfsprogs reiser4progs
vAPTinstall_return=$?
if [ $vAPTinstall_return -ne 0 ]; then
  exit $vAPTinstall_return
fi

echo ''
apt -y install xfsprogs xfsdump jfsutils
vAPTinstall_return=$?
if [ $vAPTinstall_return -ne 0 ]; then
  exit $vAPTinstall_return
fi

echo ''
apt -y install ntfs-3g
vAPTinstall_return=$?
if [ $vAPTinstall_return -ne 0 ]; then
  exit $vAPTinstall_return
fi

echo ''
vDISTcodename=$(cat /etc/os-release | grep VERSION_CODENAME | cut -d= -f2)
if [ $vDISTcodename == "buster" ]; then
  apt -y install exfat-fuse exfat-utils
  vAPTinstall_return=$?
else
  apt -y install exfat-fuse exfatprogs
  vAPTinstall_return=$?
fi;
if [ $vAPTinstall_return -ne 0 ]; then
  exit $vAPTinstall_return;
fi;

echo ''
apt -y install fdisk
vAPTinstall_return=$?
if [ $vAPTinstall_return -ne 0 ]; then
  exit $vAPTinstall_return
fi

echo ''
apt -y install gpart kpartx mtools dmraid
vAPTinstall_return=$?
if [ $vAPTinstall_return -ne 0 ]; then
  exit $vAPTinstall_return
fi

echo ''
apt -y install acl attr quota
vAPTinstall_return=$?
if [ $vAPTinstall_return -ne 0 ]; then
  exit $vAPTinstall_return
fi

echo ''
apt -y install disktype
vAPTinstall_return=$?;
if [ $vAPTinstall_return -ne 0 ]; then
  exit $vAPTinstall_return
fi

echo ''
apt -y install libzip4 libminizip1 liblz1
vAPTinstall_return=$?
if [ $vAPTinstall_return -ne 0 ]; then
  exit $vAPTinstall_return
fi

echo ''
apt -y install rsync
vAPTinstall_return=$?;
if [ $vAPTinstall_return -ne 0 ]; then
  exit $vAPTinstall_return
fi

echo ''
apt -y install jigdo-file
vAPTinstall_return=$?;
if [ $vAPTinstall_return -ne 0 ]; then
  exit $vAPTinstall_return
fi

exit 0

