#!/bin/bash

if [ $EUID -ne 0 ]; then
  echo 'Error, this script need be run as root.'
  exit 1
fi

apt -y purge libreoffice*
vAPTpurge_return=$?
if [ $vAPTpurge_return -ne 0 ]; then
  exit $vAPTpurge_return
fi
if [ -e /home/user/.config/libreoffice ]; then
  echo ''
  rm -rfv /home/user/.config/libreoffice
fi

echo ''
apt -y autoremove
vAPTautoremove_return=$?
if [ $vAPTautoremove_return -ne 0 ]; then
  exit $vAPTautoremove_return
fi

exit 0

