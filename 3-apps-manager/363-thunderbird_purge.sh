#!/bin/bash

if [ $EUID -ne 0 ]; then
  echo 'Error, this script need be run as root.'
  exit 1
fi

echo ''
apt -y purge thunderbird*
vAPTpurge_return=$?
if [ $vAPTpurge_return -ne 0 ]; then
  exit $vAPTpurge_return
fi
if [ -e /home/user/.thunderbird ]; then
  echo ''
  rm -rfv /home/user/.thunderbird
fi

echo ''
apt -y autoremove
vAPTautoremove_return=$?
if [ $vAPTautoremove_return -ne 0 ]; then
  exit $vAPTautoremove_return
fi

exit 0

