#!/bin/bash

if [ $EUID -ne 0 ]; then
  echo 'Error, this script need be run as root.'
  exit 1
fi

dpkg --status seamonkey-mozilla-build
vDPKGstatus_return=$?
if [ $vDPKGstatus_return == 0 ]; then
  apt -y purge seamonkey-mozilla-build
  vAPTpurge_return=$?
  if [ $vAPTpurge_return -ne 0 ]; then
    exit $vAPTpurge_return
  fi
fi

if [ -e /home/user/.mozilla/seamonkey ]; then
  echo ''
  rm -rfv /home/user/.mozilla/seamonkey
fi

echo ''
apt -y autoremove
vAPTautoremove_return=$?
if [ $vAPTautoremove_return -ne 0 ]; then
  exit $vAPTautoremove_return
fi

exit 0

