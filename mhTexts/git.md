## Git Brief

Simple Git commands.  

##### Install Git

~~~
sudo apt install git
git --version
~~~

##### Git global setup

~~~
git config --global user.name "YourName"
git config --global user.email "you@gmail.com"
~~~


##### Git clone

~~~
git clone https://gitlab.com/myhorses/mhscripts.git
~~~

##### Create "main" branch and add README

~~~
git switch --create main
touch README.md
git add README.md
git commit -m "add README"
git push --set-upstream origin main
~~~

##### Git pull

~~~
git pull origin main
~~~

##### Git commit

~~~
git add *
git commit -m "commit"
git push origin main
~~~
