## mhTexts

Cake recipes used by Raymond Risko and yours collaborators.  

Do not use these commands because it can damage your system.  

This is provided "AS IS", without warranty of any kind.  

---

Copyright (c) 2024 Raymond Risko
