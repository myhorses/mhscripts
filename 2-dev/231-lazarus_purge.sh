#!/bin/bash

if [ $EUID -ne 0 ]; then
  echo 'Error, this script need be run as root.'
  exit 1
fi

apt -y purge --no-install-recommends lazarus lazarus-*
vAPTpurge_return=$?
if [ $vAPTpurge_return -ne 0 ]; then
  exit $vAPTpurge_return
fi

echo ''
rm -rfv /etc/lazarus
rm -rfv /usr/share/lazarus
rm -rfv /root/.lazarus
rm -rfv /home/user/.lazarus

apt -y purge --no-install-recommends fpc*
vAPTpurge_return=$?
if [ $vAPTpurge_return -ne 0 ]; then
  exit $vAPTpurge_return
fi

echo ''
rm -rfv /etc/fppkg
rm -rfv /etc/fpc.cfg
rm -rfv /etc/fppkg.cfg
rm -rfv /usr/lib/fpc

echo ''
apt -y autoremove
vAPTautoremove_return=$?
if [ $vAPTautoremove_return -ne 0 ]; then
  exit $vAPTautoremove_return
fi

echo ''
echo 'Ok, Lazarus and FPC has been purged.'

exit 0

